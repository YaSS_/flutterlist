final String ID_COLUMN = "_id";
final String TYPE_CYCLE_COLUMN = "type";
final String NUMBER_CYCLE_COLUMN = "number";
final String PART_CYCLE_COLUMN = "part";
final String AMOUNT_CYCLE_COLUMN = "amount";
final String DATE_CYCLE_COLUMN = "date";

class Cycle {
  int id;
  String type;
  String number;
  String part;
  String amount;
  String date;

  Cycle({this.id, this.type, this.number, this.part, this.amount, this.date});

  Map<String, dynamic> toDatabaseMap() {
    return {
      ID_COLUMN: id,
      TYPE_CYCLE_COLUMN: type,
      NUMBER_CYCLE_COLUMN: number,
      PART_CYCLE_COLUMN: part,
      AMOUNT_CYCLE_COLUMN: amount,
      DATE_CYCLE_COLUMN: date
    };
  }

  factory Cycle.fromDatabaseMap(Map<String, dynamic> map) {
    return Cycle(
        id: map[ID_COLUMN],
        type: map[TYPE_CYCLE_COLUMN],
        number: map[NUMBER_CYCLE_COLUMN],
        part: map[PART_CYCLE_COLUMN],
        amount: map[AMOUNT_CYCLE_COLUMN],
        date: map[DATE_CYCLE_COLUMN]);
  }
}

class CycleType {
  final String _value;

  const CycleType._internal(this._value);

  static const TENNIS = const CycleType._internal("Tennis");
  static const FOOTBALL = const CycleType._internal("Football");
  static const HOCKEY = const CycleType._internal("Hockey");
  static const BASKETBALL = const CycleType._internal("Basketball");

  static List<CycleType> get values => [TENNIS, FOOTBALL, HOCKEY, BASKETBALL];

  toString() => this._value;

  static CycleType getTypeByValue(String value) {
    if (value == TENNIS.toString()) {
      return TENNIS;
    } else if(value == FOOTBALL.toString()) {
      return FOOTBALL;
    } else if(value == BASKETBALL.toString()) {
      return BASKETBALL;
    } else if(value == HOCKEY.toString()) {
      return HOCKEY;
    }
  }
}
