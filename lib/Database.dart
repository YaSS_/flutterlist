import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import 'Cycle.dart';

final String TABLE_NAME = "cycles";
final String KEY_BALANCE = "balance";

class DatabaseImpl implements IDatabase {
  static IDatabase _database;
  Future<Database> _flutterDao;

  DatabaseImpl._internal() {
    _flutterDao = _openDatabase();
  }

  factory DatabaseImpl.getInstance() {
    if (_database == null) {
      _initDatabase();
    }
    return _database;
  }

  static _initDatabase() {
    _database = DatabaseImpl._internal();
  }

  Future<Database> _openDatabase() async {
    return openDatabase(
      join(await getDatabasesPath(), 'database.db'),
      onCreate: (db, version) {
        return db.execute(
          "CREATE TABLE $TABLE_NAME("
              "$ID_COLUMN INTEGER PRIMARY KEY AUTOINCREMENT, "
              "$TYPE_CYCLE_COLUMN TEXT, "
              "$NUMBER_CYCLE_COLUMN TEXT, "
              "$PART_CYCLE_COLUMN TEXT, "
              "$AMOUNT_CYCLE_COLUMN TEXT, "
              "$DATE_CYCLE_COLUMN TEXT)",
        );
      },
      // Set the version. This executes the onCreate function and provides a
      // path to perform database upgrades and downgrades.
      version: 1,
    );
  }

  @override
  Future<List<Cycle>> getCycles() async {
    final Database db = await _flutterDao;
    final List<Map<String, dynamic>> maps = await db.query(TABLE_NAME);
    return List.generate(maps.length, (i) {
      return Cycle.fromDatabaseMap(maps[i]);
    });
  }

  @override
  saveCycle(Cycle cycle) async {
    final Database db = await _flutterDao;
    await db.insert(TABLE_NAME, cycle.toDatabaseMap(),
        conflictAlgorithm: ConflictAlgorithm.replace);
  }
}

abstract class IDatabase {
  saveCycle(Cycle cycle);

  Future<List<Cycle>> getCycles();
}
