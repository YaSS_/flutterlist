import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'Cycle.dart';
import 'Database.dart';

class CycleListScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return CycleListScreenState();
  }
}

class CycleListScreenState extends State<CycleListScreen> {
  TextEditingController _cycleController = TextEditingController();
  TextEditingController _partOfCycleController = TextEditingController();
  TextEditingController _amountController = TextEditingController();

  List<Cycle> _cycleList = [];
  DatabaseImpl db;
  String balance = "0";
  CycleType cycleType;

  @override
  void initState() {
    super.initState();
    db = DatabaseImpl.getInstance();
    db.getCycles().then((list) {
      _cycleList = list;
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("CycleBet")),
      body: ListView(
        children: _buildCycleList(),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => {_addNewCycle()},
        child: Icon(Icons.add),
        backgroundColor: Colors.blue,
      ),
    );
  }

  Future<void> _addNewCycle() {
    return showDialog<void>(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Add new cycle'),
            content: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  DropdownButton<CycleType>(
                    value: cycleType,
                    onChanged: (CycleType type) {
                      setState(() {
                      cycleType = type;
                      });
                    },
                    hint: Text("Please choose type"),
                    items: CycleType.values.map((type) {
                      return DropdownMenuItem(
                        child: Text(type.toString()),
                        value: type,
                      );
                    }).toList(),
                  ),
                  TextField(
                      keyboardType: TextInputType.number,
                      controller: _cycleController,
                      textInputAction: TextInputAction.done,
                      decoration: InputDecoration(hintText: 'cycle')),
                  TextField(
                      keyboardType: TextInputType.number,
                      controller: _partOfCycleController,
                      textInputAction: TextInputAction.done,
                      decoration: InputDecoration(hintText: 'part of cycle')),
                  TextField(
                      keyboardType: TextInputType.number,
                      controller: _amountController,
                      textInputAction: TextInputAction.done,
                      decoration: InputDecoration(hintText: 'amount'))
                ],
              ),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text('Add'),
                onPressed: () => {_saveNewCycle()},
              )
            ],
          );
        });
  }

  _saveNewCycle() {
    if (_amountController.text.isNotEmpty &&
        _partOfCycleController.text.isNotEmpty &&
        _cycleController.text.isNotEmpty &&
        cycleType != null) {
      Cycle cycle = new Cycle(
          id: null,
          type: cycleType.toString(),
          number: _cycleController.text,
          part: _partOfCycleController.text,
          amount: _amountController.text,
          date: DateFormat("hh:mm").format(DateTime.now()));

      _cycleList.add(cycle);
      db.saveCycle(cycle);
      setState(() {});

      _cycleController.clear();
      _partOfCycleController.clear();
      _amountController.clear();
      cycleType = null;

      Navigator.of(context).pop();
    }
  }

  List<Widget> _buildCycleList() {
    return _cycleList.map((cycle) => _getCycleTile(cycle)).toList();
  }

  Widget _getCycleTile(Cycle cycle) {
    var image;
    switch (CycleType.getTypeByValue(cycle.type)) {
      case CycleType.BASKETBALL:
        image = "assets/ic_basketball.png";
        break;
      case CycleType.TENNIS:
        image = "assets/ic_tennis.png";
        break;
      case CycleType.FOOTBALL:
        image = "assets/ic_football.png";
        break;
      case CycleType.HOCKEY:
        image = "assets/ic_hockey.png";
        break;
    }
    return Card(
      color: Colors.grey,
      elevation: 8,
      margin: EdgeInsets.fromLTRB(10, 5, 10, 5),
      child: ListTile(
        leading: SizedBox(
          height: double.infinity,
          child: Container(
            padding: EdgeInsets.only(right: 12),
            decoration: BoxDecoration(
                border:
                    Border(right: BorderSide(width: 1.0, color: Colors.white24))),
            child: Image.asset(
              image,
              width: 35,
              height: 35,
            ),
          ),
        ),
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
                margin: EdgeInsets.fromLTRB(0, 2, 5, 2),
                child: Text(
                  "Cycle: ${cycle.number}",
                  style: TextStyle(fontSize: 17, color: Colors.white),
                )),
            Container(
              margin: EdgeInsets.fromLTRB(5, 2, 5, 2),
              child: Text(
                "Part: ${cycle.part}",
                style: TextStyle(fontSize: 17, color: Colors.white),
              ),
            )
          ],
        ),
        subtitle: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(top: 5),
              child: Text(
                "Amount ${cycle.amount}",
                style: TextStyle(color: Colors.white),
              ),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(0, 5, 0, 5),
              child: Text(
                "${cycle.date}",
                style: TextStyle(color: Colors.white),
              ),
            )
          ],
        ),
      ),
    );
  }
}
